# Fokus plasmoid

Fokus is simple pomodoro KDE Plasma plasmoid.

![](screenshots/1.png)

To install use:
kpackagetool6 -t Plasma/Applet -i package

Plasma 5
https://www.pling.com/p/1308861/

Plasma 6:
https://www.pling.com/p/2117117/

